from urllib.request import urlopen

def found_test(my_str):
    url = urlopen("https://the-internet.herokuapp.com/context_menu")
    output = url.read().decode('utf-8')
    if my_str in output:
        return True
    return False

def test_right_click():
    assert found_test("Right-click in the box below to see one called 'the-internet'"), True

def test_alibaba():
    assert found_test("Alibaba"), False
